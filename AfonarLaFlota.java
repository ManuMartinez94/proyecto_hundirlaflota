package joc1;

import java.util.Scanner;

public class AfonarLaFlota {

    public static void main(String[] args) {
        mostrar_text("BENVINGUT A AFONAR LA FLOTA!");
        iniciar_partida(menu());
    }

//************************************************
//*****************MENU***************************
//************************************************ 
    //Mostra el menú fins que s'introduïsca una dada vàlida i retorna l'opció triada
    public static int menu() {
        int nivell = 0;
        while (nivell == 0) {
            mostrar_text("**********Menu**********");
            mostrar_text("|  1-Nivell fàcil.     |");
            mostrar_text("|  2-Nivell mitjà.     |");
            mostrar_text("|  3-Nivell difícil.   |");
            mostrar_text("|  4-Personalitzat.    |");
            mostrar_text("|  5-Eixir             |");
            mostrar_text("************************");
            mostrar_text("Introdueix l'opció: ");
            nivell = demanar_int();
            if(validar_int(nivell,1,5)){
                break;
            }
            nivell=0;
        }
        return nivell;
    }

//************************************************
//*****************PARTIDA************************
//************************************************  
    //Redirigeix l'opció triada amb la seua funcionalitat, si prem el 5, ix del programa
    public static void iniciar_partida(int nivell) {
        char[][] tauler = crear_tauler(10, 10);
        switch (nivell) {
            case 1:
                inserir_vaixells(tauler, 5, 3, 1, 1);
                partida(tauler, 50);
                break;
            case 2:
                inserir_vaixells(tauler, 2, 1, 1, 1);
                partida(tauler, 30);
                break;
            case 3:
                inserir_vaixells(tauler, 1, 1, 0, 0);
                partida(tauler, 10);
                break;
            case 4:
                menu_partida_personalitzada();
                break;
            case 5:
                mostrar_text("Adéu!");
                System.exit(0);
        }
    }

    //Demana a l'usuari en bucle que dispare fins que s'acaben els intents o afone tots els vaixells. En acabar, torna al menú principal
    public static void partida(char[][] tauler, int intents) {
        boolean victoria = false;
        while (intents != 0) {
            mostrar_tauler_usuari(tauler);
            mostrar_text("Intents: "+intents);
            tauler = disparar(tauler, dispar(tauler));
            if (comprovar_victoria(tauler)) {
                victoria = true;
                break;
            }
            intents--;
        }
        mostrar_tauler(tauler);
        if (victoria) {
            mostrar_text("Has guanyat!");
        } else {
            mostrar_text("Has perdut!!!");
        }
        iniciar_partida(menu());
    }
    
//************************************************
//*****************PERSONALITZADA*****************
//************************************************ 
    //Menú de la partida personalitzada que demana totes les dades a l'usuari i inicia la partida
    public static void menu_partida_personalitzada() {
        char[][] tauler;
        int[] medides_tauler;
        int[] vaixells;
        int intents_maxims;
        int caselles;
        mostrar_text("*****PARTIDA PERSONALITZADA*****");
        medides_tauler = tauler_partida_personalitzada();
        tauler = crear_tauler(medides_tauler[0],medides_tauler[1]);
        caselles=tauler.length*tauler[0].length;
        vaixells = vaixells_partida_personalitzada(tauler,caselles);
        intents_maxims = intents_partida_personalitzada(caselles,vaixells);
        
        inserir_vaixells(tauler,vaixells[0],vaixells[1],vaixells[2],vaixells[3]);
        partida(tauler,intents_maxims);
    }
    //Demana les medides del tauler fins que son vàlids i les retorna en un array 
    public static int[] tauler_partida_personalitzada(){
        boolean fet = false;
        int[] tauler = new int[2];
        while(!fet){
            mostrar_text("Introdueix el nombre de files(minim 6 i màxim 26):");
            tauler[0]=demanar_int();
            mostrar_text("Introdueix el nombre de columnes(minim 6 i màxim 26):");
            tauler[1]=demanar_int();
            if (validar_int(tauler[0], 6, 26) && validar_int(tauler[1], 6, 26)) {
                fet = true;
            }
        }
        return tauler;
    }
    
    //Demana el nombre de vaixells de cada tipus fins que són vàlids
    public static int[] vaixells_partida_personalitzada(char[][]tauler,int caselles){
        boolean fet = false;
        int[] vaixells = new int[4];
        int[] vaixells_maxims = new int[]{5,3,1,1};
        vaixells_maxims = calcular_nombre_vaixells_partida_personalitzada(caselles,vaixells_maxims);
        while(!fet){
            mostrar_text("Llanxes(minim 1 i màxim "+vaixells_maxims[0]+"):");
            vaixells[0]=demanar_int();
            mostrar_text("Vaixells(màxim "+vaixells_maxims[1]+"):");
            vaixells[1]=demanar_int();
            mostrar_text("Cuirassats(màxim "+vaixells_maxims[2]+"):");
            vaixells[2]=demanar_int();
            mostrar_text("Portaavions(màxim "+vaixells_maxims[3]+"):");
            vaixells[3]=demanar_int();
            if(validar_vaixells_partida_personalitzada(vaixells,vaixells_maxims)){
                fet = true;
            }
        }
        return vaixells;
    }
    
    //Comprova el nombre de vaixells introduïts
    public static boolean validar_vaixells_partida_personalitzada(int[]vaixells, int[]vaixells_maxims){
        for (int i = 0; i < 4; i++) {
            if(i==0){
                if(!validar_int(vaixells[i],1,vaixells_maxims[i])){
                    return false;
                }
            }else{
                if(!validar_int(vaixells[i],0,vaixells_maxims[i])){
                    return false;
                }
            }
        }
        return true;
    }
    
    //Calcula el nombre màxim de vaixells de cada tipus que es poden posar depenent de les dimensions del tauler
    public static int[] calcular_nombre_vaixells_partida_personalitzada(int caselles,int[]vaixells){
        int[]medides = new int[]{64,100,144,196,256,324,400,484,576,676,800};
        for (int i = 0; i < medides.length; i++) {
            if(caselles>=medides[i]&&caselles<medides[i+1]){
                for (int j = 0; j < vaixells.length; j++) {
                    vaixells[j]+=(i+1);
                }
            }
        }
        return vaixells;
    }
    
    //Demana el nombre d'intents fins que són vàlids. El nombre depén de les dimensions del tauler i el nombre de vaixells
    public static int intents_partida_personalitzada(int caselles,int[]vaixells){
        boolean fet = false;
        int intents=0;
        while(!fet){
            mostrar_text("Intents(minim "+suma_vaixells(vaixells)+" i maxim "+caselles+"):");
            intents = demanar_int();
            if(validar_int(intents,suma_vaixells(vaixells),caselles)){
                fet = true;
            }
        }
        return intents;
    }
        
//************************************************
//*****************TAULER*************************
//************************************************    
    //Crea un tauler amb les mesures que li passem
    public static char[][] crear_tauler(int y, int x) {
        char[][] tauler = new char[y][x];
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                tauler[i][j] = '-';
            }
        }
        return tauler;
    }

    //Imprimeix el tauler per pantalla
    public static void mostrar_tauler(char[][] tauler) {
        String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int num = 0;
        String linea = "  ";
        for (int i = 0; i < tauler[0].length; i++) {
            if(num>9){
               linea += " " + num; 
            }else{
               linea += " " + num + " "; 
            }
            num++;
        }
        mostrar_text(linea);
        linea = "";
        for (int j = 0; j < tauler.length; j++) {
            linea += abc.charAt(j) + " ";
            for (int k = 0; k < tauler[j].length; k++) {
                linea += " " + tauler[j][k] + " ";
            }
            mostrar_text(linea);
            linea = "";
        }
    }

    //Imprimeix el tauler de l'usuari per pantalla(canvia les lletres dels vaixells pero '-')
    public static void mostrar_tauler_usuari(char[][] tauler) {
        String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int num = 0;
        String linea = "  ";
        for (int i = 0; i < tauler[0].length; i++) {
            if(num>9){
               linea += " " + num; 
            }else{
               linea += " " + num + " "; 
            }
            num++;
        }
        mostrar_text(linea);
        linea = "";
        for (int j = 0; j < tauler.length; j++) {
            linea += abc.charAt(j) + " ";
            for (int k = 0; k < tauler[j].length; k++) {
                switch (tauler[j][k]) {
                    case 'L':
                        linea += " - ";
                        break;
                    case 'B':
                        linea += " - ";
                        break;
                    case 'Z':
                        linea += " - ";
                        break;
                    case 'P':
                        linea += " - ";
                        break;
                    default:
                        linea += " " + tauler[j][k] + " ";
                }
            }
            mostrar_text(linea);
            linea = "";
        }
    }

    //Comprova si queden vaixells al tauler
    public static boolean comprovar_victoria(char[][] tauler) {
        for (int i = 0; i < tauler.length; i++) {
            for (int j = 0; j < tauler[i].length; j++) {
                if (tauler[i][j] != '-' && tauler[i][j] != 'X' && tauler[i][j] != 'A') {
                    return false;
                }
            }
        }
        return true;
    }
    
//************************************************
//*****************VAIXELLS***********************
//************************************************ 
    //Inserida els vaixells
    public static void inserir_vaixells(char[][] tauler, int num_llanxa, int num_vaixell, int num_cuirassat, int num_portaavions) {
        for (int i = 0; i < num_portaavions; i++) {
            inserir_vertical(tauler, 'P', 5);
        }
        for (int j = 0; j < num_cuirassat; j++) {
            inserir_horitzontal(tauler, 'Z', 4);
        }
        for (int k = 0; k < num_vaixell; k++) {
            inserir_horitzontal(tauler, 'B', 3);
        }
        for (int m = 0; m < num_llanxa; m++) {
            inserir_horitzontal(tauler, 'L', 1);
        }
    }

    //Inserida aleatòriament el vaixell de forma horitzontal
    public static void inserir_horitzontal(char[][] tauler, char lletra, int llargaria) {
        int y;
        int x;
        boolean inserit = false;

        while (inserit == false) {
            y = (int) (Math.random() * tauler.length);
            x = (int) (Math.random() * tauler[0].length);
            if (comprova_horitzontal(tauler, y, x, llargaria)) {
                for (int i = 0; i < llargaria; i++) {
                    tauler[y][x + i] = lletra;
                }

                inserit = true;
            }
        }
    }

    //Inserida aleatòriament el vaixell de forma vertical
    public static void inserir_vertical(char[][] tauler, char lletra, int llargaria) {
        int y;
        int x;
        boolean inserit = false;

        while (inserit == false) {
            y = (int) (Math.random() * tauler.length);
            x = (int) (Math.random() * tauler[0].length);
            if (comprova_vertical(tauler, y, x, llargaria)) {
                for (int i = 0; i < llargaria; i++) {
                    tauler[y + i][x] = lletra;
                }
                inserit = true;
            }
        }
    }

    //Comprova si el vaixell cap horitzontalment en les coordenades
    public static boolean comprova_horitzontal(char[][] tauler, int y, int x, int llargaria) {
        if ((tauler[y].length - x) >= llargaria) {
            for (int i = 0; i < llargaria; i++) {
                if (tauler[y][x + i] != ('-')) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
    
    //Comprova si el vaixell cap verticalment en les coordenades
    public static boolean comprova_vertical(char[][] tauler, int y, int x, int llargaria) {
        if ((tauler.length - y) >= llargaria) {
            for (int i = 0; i < llargaria; i++) {
                if (tauler[y + i][x] != ('-')) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

//************************************************
//*****************DISPARS***********************
//************************************************ 
    //Demana les coordenades del dispar a l'usuari fins que siguen vàlides i retorna les coordenades
    public static int[] dispar(char[][] tauler) {
        String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String y;
        int x;
        boolean valid = false;
        int[] coordenada_dispar = new int[2];

        while (valid == false) {
            mostrar_text("Introdueix la lletra de la fila: ");
            y = demanar_string().toUpperCase();
            if (comprovar_lletra(tauler.length, y)) {
                coordenada_dispar[0] = abc.indexOf(y);
                valid = true;
            }else{
                mostrar_text("Lletra no vàlida.");
            }
        }

        valid = false;
        while (valid == false) {
            mostrar_text("Introdueix el nombre de la columna: ");
            x = demanar_int();
            if (validar_int(x,0,tauler[0].length)) {
                coordenada_dispar[1] = x;
                valid = true;
            } else {
                mostrar_text("Nombre no vàlid.");
            }
        }

        return coordenada_dispar;
    }

    //Dispara en les coordenades indicades i canvia la lletra depenent si és aigua o un vaixell
    public static char[][] disparar(char[][] tauler, int[] coordenada_dispar) {
        int y = coordenada_dispar[0];
        int x = coordenada_dispar[1];
        switch (tauler[y][x]) {
            case '-':
                tauler[y][x] = 'A';
                mostrar_text("Aigua!");
                break;
            case 'A':
                mostrar_text("Has tornat a disparar al mateix lloc, perds un intent, fixat!");
                break;
            case 'X':
                mostrar_text("Has tornat a disparar al mateix lloc, perds un intent, fixat!");
                break;
            default:
                tauler[y][x] = 'X';
                mostrar_text("Tocat!");
        }
        return tauler;
    }
    
    //Comprova si la lletra és vàlida(si la posició és igual que la fila o si ha introduït una lletra vàlida) 
    public static boolean comprovar_lletra(int files, String y) {
        String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (abc.indexOf(y) > files || abc.indexOf(y) == -1) {
            return false;
        }
        return true;
    }
    
//************************************************
//**************FUNCIONS AUXILIARS****************
//************************************************  
    //Imprimeix el text que li passem per paràmetre
    public static void mostrar_text(String text){
        System.out.println(text);
    }
    
    //Retorna el nombre total de caselles ocupades pels vaixells
    public static int suma_vaixells(int[]vaixells){
        int total=0;
        for (int i = 0; i < 4; i++) {
            switch(i){
                case 0:total+=1*vaixells[i];break;
                case 1:total+=3*vaixells[i];break;
                case 2:total+=4*vaixells[i];break;
                case 3:total+=5*vaixells[i];break;   
            }
        }
        return total;
    }
    
    //Funció que comprova si els nombres introduïts estan dins del rang introduït
    public static boolean validar_int(int num, int min, int max) {
        if (num >= min && num <= max) {
            return true;
        } else {
            return false;
        }
    }
    
    //Demana a l'usuario un nombre enter amb Scanner
    public static int demanar_int(){
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }
    
    //Demana a l'usuari un String amb Scanner
    public static String demanar_string(){
        Scanner s = new Scanner(System.in);
        return s.next();
       
    }
}
